package ictgradschool.web.lab09.ex01;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ArticleServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        String author = request.getParameter("author");
        String genre = request.getParameter("genre");
        String content = request.getParameter("content");
        String[] array = new String[]{"four", "three", "two", "one"};



        // printWriter is used to write to the HTML document
        PrintWriter out = response.getWriter();

        out.println("<h1>" + title + "</h1>");
        out.println("<p>by " + author +"<br>Genre: " +genre + "</p>");
        out.println("<p>" + content + "</p>");
        out.println("<h2>A string array: </h2><ul>");
        for (int i = 0; i < array.length; i++) {
            out.println("<li>" + array[i] +"</li>");
        }
        out.println("</ul>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



    }
}
