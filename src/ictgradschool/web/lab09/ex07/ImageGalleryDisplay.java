package ictgradschool.web.lab09.ex07;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class ImageGalleryDisplay extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletContext servletContext = getServletContext();

        // this code which we were given doesn't seem to work because the result comes back missing a WEB-INF, so wrote it manually below
        String fullPhotoPath = servletContext.getRealPath("/Photos");
        //String fullPhotoPath = "C:\\Users\\smh30\\Desktop\\Industry Labs\\2018s_web_lab_09\\out\\artifacts\\web_lab_09\\WEB-INF\\Photos\\";
        PrintWriter out = response.getWriter();


        //out.println("fullPhotoPath = " + fullPhotoPath);

        File folder = new File(fullPhotoPath);
        if (folder.exists()) {


            File[] listOfPhotos = folder.listFiles();
            if (listOfPhotos.length == 0) {
                out.println("<p> No images available </p>");
            }


            String thumb = "_thumbnail.png";
out.println("<table>");
out.println("<thead><th>Thumbnail</th><th>Filename</th><th>File-size</th></thead>");
            for (int i = 0; i < listOfPhotos.length; i++) {

                if (listOfPhotos[i].getName().contains(thumb)) {

                    String photoName = listOfPhotos[i].getName();


                    // print the name of the photo minus _thumb.png and underscores
                    String printName = photoName.substring(0, photoName.indexOf(thumb));
                    String printNameFixed = printName.replace('_', ' ');
                    long fileSizeBytes = 0;
                    int fileSizeKB = 0;

                    // print the file-size of the full file
                    String fullFileName = printName + ".jpg";
                    //out.println(fullFileName);
                    for (int j = 0; j < listOfPhotos.length; j++) {
                        if (listOfPhotos[j].getName().equals(fullFileName)) {

                            fileSizeBytes = listOfPhotos[j].length();

                            fileSizeKB = (int) (fileSizeBytes / 1024);
                        }
                    }

                    out.println("<tr>");

                    out.println("<td><a href=\"..\\Photos\\" + fullFileName + "\">" + "<img src=\"..\\Photos\\" + photoName + "\"></a></td>");
                    out.println("<td>" + printNameFixed + "</td>");
                    out.println("<td>" + fileSizeKB + " kb</td>");

                    out.println("</tr>");


                }
            }
            out.println("</table>");
        } else {
            out.println("<p> no such image folder found </p>");
        }
    }


}
